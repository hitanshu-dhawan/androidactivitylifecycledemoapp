package com.example.activitylifecycledemoapp

import android.os.Bundle

class B_Activity : BaseActivity(TAG = "B_Activity") {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_b)

        //...
    }

}