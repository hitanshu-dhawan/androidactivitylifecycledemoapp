package com.example.activitylifecycledemoapp

import android.content.Intent
import android.os.Bundle
import android.widget.Button

class A_Activity : BaseActivity(TAG = "A_Activity") {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_a)

        findViewById<Button>(R.id.button).setOnClickListener {
            startActivity(Intent(this, B_Activity::class.java))
        }
    }

}